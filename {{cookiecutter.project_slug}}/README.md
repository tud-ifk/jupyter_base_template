[![version]({{cookiecutter.remote_pages_base_url}}{{cookiecutter.project_slug}}/version.svg)][static-gl-url] [![pipeline]({{cookiecutter.gitlab_base_url}}{{cookiecutter.project_slug}}/badges/main/pipeline.svg)][static-gl-url]

# {{cookiecutter.project_name}}

![Example image](resources/{{cookiecutter.project_slug}}.png?raw=true)

{{cookiecutter.description}}

Links to HTML-converted Notebooks:
- [Notebook 1][1]

Use `--recursive`, to clone the repository including its submodules:
```
git clone --recursive {{cookiecutter.git_repository}}
```

## Developers

- This repository is versioned with 
  [python-semantic-release](https://python-semantic-release.readthedocs.io/en/latest/),
- Jupyter notebooks are tracked as Markdown files using [Jupytext](https://github.com/mwouts/jupytext).
- If you want to run these notebooks, have a look at the instructions to use the 
  [Carto-Lab Docker](https://gitlab.vgiscience.de/lbsn/tools/jupyterlab), 
  provided at the beginning of the [notebook][1].

To manually bump a version:
```python
semantic-release publish
```

To create `ipynb` files from Markdown:
```
conda activate jupyter_env
jupytext --sync /home/jovyan/work/md/notebook.md
```

This will create notebooks that can be opened in JupyterLab in the subfolder [notebooks/](notebooks/).

[1]: {{cookiecutter.remote_pages_base_url}}{{cookiecutter.project_slug}}/html/notebook.html
[static-gl-url]: {{cookiecutter.gitlab_base_url}}{{cookiecutter.project_slug}}
