#!/bin/bash

# abort on first error
set -e

echo "Git repository config.. "

git init
git config --local core.filemode false
git config --local push.default current
git config --local pull.default current
git checkout -b main
# hardcoded base modules repository
git submodule add git@gitlab.hrz.tu-chemnitz.de:s7398234--tu-dresden.de/base_modules.git py/modules/base
git add .
git commit -m 'Initial commit'
git tag -a v0.1.0 -m 'Initial version tag'
git remote add origin '{{cookiecutter.git_repository}}'

if [ -z ${GL_PAT+x} ]; then
    echo "Gitlab personal access token (GL_PAT) for target repository is not set.";
    exit 1
fi

echo "Creating remote repository {{ cookiecutter.project_slug }}.. "

create_response=$(
    curl --request POST --header "PRIVATE-TOKEN: $GL_PAT" \
        --header "Content-Type: application/json" --data '{
            "name": "{{ cookiecutter.project_name }}", "description": "{{ cookiecutter.description }}", "path": "{{ cookiecutter.project_slug }}",
            "namespace_id": "20000", "initialize_with_readme": "false"}' \
        --url 'https://{{ cookiecutter.gitlab_domain }}/api/v4/projects/')

error_test=$(echo "$create_response" | jq 'has("error")')
if [ ! -n "$error_test" ] && [ "$error_test"  ] ; then
  echo "$create_response" | jq '.error_description'
  exit
fi

project_id=$(echo "$create_response" | jq '.id')

echo "Created project with ID $project_id"

echo "Retrieving Gitlab env variables from template repository .. "

# hardcoded project id (15714) of template project
json_response=$(
    curl --header "PRIVATE-TOKEN: $GL_PAT" "https://{{ cookiecutter.gitlab_domain }}/api/v4/projects/15714/variables")

# if it is an array (= starts with "["), everything ok..
if [[ ! $json_response = [* ]]; then
    # if not, check if json contains key "error"
    error_test=$(echo "$json_response" | jq 'has("error")')
    # if error_test is not empty, and true, print error_description
    if [ ! -n "$error_test" ] && [ "$error_test"  ] ; then
      echo "$json_response" | jq '.error_description'
      exit
    fi
fi

echo "Creating Gitlab env variables on target repository .. "

# in case the group-space for "tud-ifk" is used, we can use the group-level GL_TOKEN set in the template project
# otherwise, substitute with the private gitlab token
if [ ! "{{cookiecutter.gitlab_base_url}}" = "https://{{ cookiecutter.gitlab_domain}}/tud-ifk/" ]; then
    SUBSTITUTE_PRIVATE_TOKEN=true
fi

for row in $(echo "${json_response}" | jq -r '.[] | @base64'); do
    _jq() {
     echo "${row}" | base64 --decode | jq -r "${1}"
    }
    val="$(_jq '.value')"
    if [ "$(_jq '.key')" = "GL_TOKEN" ] && $SUBSTITUTE_PRIVATE_TOKEN ; then
        echo 'Using private token for CI'
        val="$GL_PAT"
    fi
    curl --silent --output /dev/null --show-error --fail --request POST --header "PRIVATE-TOKEN: $GL_PAT" \
      "https://{{ cookiecutter.gitlab_domain }}/api/v4/projects/$project_id/variables" \
      --form "variable_type=$(_jq '.variable_type')" \
      --form "key=$(_jq '.key')" \
      --form "value=$val" \
      --form "protected=$(_jq '.protected')" \
      --form "raw=$(_jq '.raw')" \
      --form "environment_scope=$(_jq '.environment_scope')"
done

GREEN='\033[0;32m'
NC='\033[0m' # No Color

echo -e "Done. Check the repository here: \n{{cookiecutter.gitlab_base_url}}{{cookiecutter.project_slug}}\n"
echo -e "Remaining step: Use ${GREEN}git push --follow-tags${NC} for the first time pushing to remote.\n"