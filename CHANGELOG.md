# CHANGELOG



## v0.6.2 (2023-09-19)

### Fix

* fix: semantic release no auto version ([`d100a55`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/d100a558fcc1951d1f6269236dc1afcd84f507fa))


## v0.6.1 (2023-09-07)

### Fix

* fix: set gitlab description on repository creation ([`25b36cd`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/25b36cdc8c30c6d76e9d2ee93f0185b25eeec38d))


## v0.6.0 (2023-09-07)

### Feature

* feat: migrate project and template to pyproject.toml ([`e0532f8`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/e0532f86d085c8e97e70ba979381a6496dacfdd3))


## v0.5.0 (2023-07-14)

### Documentation

* docs: Recommend cruft for continuous templating ([`2a8ea44`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/2a8ea44cf3b63b77d59b298e32ab399bbb82b145))

### Feature

* feat: Add abort on first error to script ([`1ae65c8`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/1ae65c8dd11328524cc57340193703558a72beb0))

### Fix

* fix: Update deprecated Gitlab variables ([`5f2759d`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/5f2759d29e6b2b0cada89dab7ee4f4a9c0732903))

* fix: Update deprecated Gitlab variables ([`10825e2`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/10825e2070bbcf5793c8c1f24a424596b96c887a))

### Unknown

* Clean up conf.json ([`2b5ffb4`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/2b5ffb40069db43a02adbc9440ed6812ba16778b))


## v0.4.5 (2023-04-21)

### Fix

* fix: Explicitly checkout to main branch after git init ([`6e1fe11`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/6e1fe113642c466984d85273398b0c25319e2a87))


## v0.4.4 (2023-04-20)

### Fix

* fix: jq error message in case of array ([`c46767e`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/c46767e76b1417646371e11eff74cf73c38dbd9c))


## v0.4.3 (2023-04-20)

### Documentation

* docs: Add link to notebooks in Readme by default ([`4996834`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/4996834c86606db1de58f73645b6ec3c4298fc14))

* docs: Improve grepping path from conda env ([`b7b7fe9`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/b7b7fe997bb7bb07ac42691335f1919f61c79d4a))

### Fix

* fix: Formatting ([`dd432e0`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/dd432e0e3be654b2a57f3afc1a5c84867bd8b94b))

* fix: Catch expired access token error ([`242dda0`](https://gitlab.hrz.tu-chemnitz.de/tud-ifk/jupyter_base_template/-/commit/242dda0f0cf54b8218a5920ef1c95b5be4657b36))
